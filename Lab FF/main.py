# -*- coding: utf-8 -*-
from pyb import UART
import pyb
import micropython
import Encoder
import MotorDriver
import ClosedLoop
from array import array
import time
#from math import exp, sin, pi

def generate_times(start_pos,end_pos):
    times  = array('f', list(time1/10 for time1 in range(start_pos,end_pos)))
    return times

# We need a place to store the data values as well.
def generate_values(times,total_element,encoder1,closedloop):
    #closedloop1 = ClosedLoop.ClosedLoop()
    values = array('f', total_element*[0])
    velocity = array('f',total_element*[0])
    n=0
    # We could do this with an iterator, but a for loop is OK too.
    while n < total_element: 
        now = time.ticks_ms()
        cycle = (now % 100)            
        if cycle == 0:
            values[n] = encoder1.get_position()
            delta_value = encoder1.get_delta()
            closedloop.update(delta_value)
            velocity[n] = closedloop.get_L_percent()
            n += 1
                        
        #values[n] = exp(-time/10)*sin(2*pi/3*time)
    #return values
    return values,velocity

micropython.alloc_emergency_exception_buf(100)

total_element = 151
myuart = UART(2)
encoder_no = 1

pinB6 = pyb.Pin(pyb.Pin.cpu.B6,pyb.Pin.IN)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7,pyb.Pin.IN)
tim4 = pyb.Timer(4, prescaler=0, period = 65535)  
#tch1 = tim4.channel(1,pyb.Timer.ENC_A, pin=pinB6)
#tch2 = tim4.channel(2,pyb.Timer.ENC_B, pin=pinB7)

encoder1 = Encoder.Encoder(pinB6,pinB7,tim4)

pinC6 = pyb.Pin(pyb.Pin.cpu.C6,pyb.Pin.IN)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7,pyb.Pin.IN)
tim8 = pyb.Timer(8, prescaler=0, period = 65535)    

encoder2 = Encoder.Encoder(pinC6,pinC7,tim8)

pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4,pyb.Pin.OUT_PP);
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5,pyb.Pin.OUT_PP);  
pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0,pyb.Pin.OUT_PP);
pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1,pyb.Pin.OUT_PP);  
tim = pyb.Timer(3,prescaler=0,period=65535)
    
moe1 = MotorDriver.MotorDriver(pin_nSLEEP,pin_IN1,pin_IN2,pin_IN3,pin_IN4,tim)
moe2 = MotorDriver.MotorDriver(pin_nSLEEP,pin_IN1,pin_IN2,pin_IN3,pin_IN4,tim)
# For some reason, after set the channel, the pin_IN1 and pin_IN2 is not
# output mode anymore, so we need to set it back.
pin_OUT1 = pyb.Pin(pyb.Pin.cpu.B4,pyb.Pin.OUT_PP);
pin_OUT2 = pyb.Pin(pyb.Pin.cpu.B5,pyb.Pin.OUT_PP);  
pin_OUT3 = pyb.Pin(pyb.Pin.cpu.B0,pyb.Pin.OUT_PP);
pin_OUT4 = pyb.Pin(pyb.Pin.cpu.B1,pyb.Pin.OUT_PP);  

closedloop1 = ClosedLoop.ClosedLoop()
    
tuples = []
times = []
values = []
velocity = []

times = generate_times(0,total_element)

while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        char_input = chr(val)
        if char_input == "G":
            myuart.write('Generating data Processing\n')
            if encoder_no == 1:
                #values = generate_values(times,total_element,encoder1,closedloop1)
                tuples = generate_values(times,total_element,encoder1,closedloop1)
                values = tuples[0]
                velocity = tuples[1]
            else:
                #values = generate_values(times,total_element,encoder2,closedloop1)
                tuples = generate_values(times,total_element,encoder2,closedloop1)
                values = tuples[0]
                velocity = tuples[1]
            for i in range(total_element):
                #myLineString = '{:}, {:}\r\n'.format(times[i], values[i])
                myLineString = '{:}, {:}, {:}\r\n'.format(times[i], values[i],velocity[i])
                myuart.write(myLineString)    
        elif char_input == "S":
            myuart.write('Ending Collecting Data\n')
            break
        elif char_input == "Z":
            if encoder_no == 1:
                myuart.write('Zero the Encoder 1 Position\n')
                encoder1.set_position(0)
            else:
                myuart.write('Zero the Encoder 2 Position\n')
                encoder2.set_position(0)
        elif char_input == "P":
            if encoder_no == 1:
                myuart.write('Print out the Encoder 1 Position\n')  
                myLineString = '{:}\r\n'.format(encoder1.get_position())                
            else:
                myuart.write('Print out the Encoder 2 Position\n')  
                myLineString = '{:}\r\n'.format(encoder2.get_position())                
            myuart.write(myLineString)                        
        elif char_input == "D":
            if encoder_no == 1:
                myuart.write('Print out the Encoder 1 Delta\n')        
                myLineString = '{:}\r\n'.format(encoder1.get_delta())                
            else:
                myuart.write('Print out the Encoder 2 Delta\n')        
                myLineString = '{:}\r\n'.format(encoder2.get_delta())                
            myuart.write(myLineString)     
        elif char_input == "T":
            myuart.write('Toggle Encoder\n')        
            if encoder_no == 1:
                encoder_no = 2
            else:
                encoder_no = 1
        elif char_input == "M":
            if encoder_no == 1:
                myuart.write('Start Motor 1\n')
                moe1.enable(pin_nSLEEP)
            else:
                myuart.write('Start Motor 2\n')
                moe2.enable(pin_nSLEEP)                
        elif char_input == "N":
            if encoder_no == 1:
                myuart.write('Stop Motor 1\n')
                moe1.disable(pin_nSLEEP)
            else:
                myuart.write('Stop Motor 2\n')
                moe2.disable(pin_nSLEEP)                
        elif char_input == "F":
            if encoder_no == 1:
                myuart.write('Forward Motor 1\n')
                moe1.set_duty(pin_IN1,pin_IN2,100)
            else:
                myuart.write('Forward Motor 2\n')
                moe2.set_duty(pin_IN3,pin_IN4,100)
        elif char_input == "B":
            if encoder_no == 1:
                myuart.write('Backward Motor 1\n')
                moe1.set_duty(pin_IN1,pin_IN2,-100)
            else:
                myuart.write('Backward Motor 2\n')
                moe2.set_duty(pin_IN3,pin_IN4,-100)
        else:
            myuart.write('Finished')
