# -*- coding: utf-8 -*-
from matplotlib import pyplot
from array import array
import csv
import serial
    
def plot_graph(times,values,desc,title):
    pyplot.figure()
    pyplot.plot(times, values)
    pyplot.xlabel('Time, t[100ms]')
    pyplot.ylabel(desc)
    pyplot.title(title)
    
def create_csv(times,values,velocity,total_generate):
    with open ('time.csv','w',newline='') as csvfile:
        spamwriter = csv.writer(csvfile,delimiter=',',quotechar='/',quoting=csv.QUOTE_MINIMAL)
        for row in range(0,total_generate):
            spamwriter.writerow([times[row],values[row],velocity[row]])  

def calculate_j(values,velocity,total_element):
    j_sum = 0
    theta_ref = (max(velocity) + min(velocity)) / 2
    for i in range(total_element):
        omega = pow(values[i],2)
        theta = pow((theta_ref - velocity[i]),2)
        j_sum += omega + theta
    j = j_sum / total_element    
    return j

def sendChar(inv):
    ser.write(str(inv).encode('ascii'))
        
def getData():
    timeout = 0
    while ser.in_waiting == 0:
        timeout+=1
        if timeout > 1_000_000:
            return None
    myval = ser.readline().decode('ascii')
    return myval
        
ser = serial.Serial(port='COM6',baudrate=115273,timeout=1)

total_element = 151
times = array('f', total_element*[0])
values = array('f', total_element*[0])
velocity = array('f', total_element*[0])

while True:
    print("Menu Choice:")
    print("")
    print("Z = Zero the encoder 1 position")
    print("P = Print out the encoder 1 position")
    print("D = Print out the encoder 1 delta")
    print("G = Collect encoder 1 data for 30 seconds and generate a plot")
    print("S = End data collection prematurely")
    print('T = Toggle Encoder/Motor')
    print("M = Start Motor")
    print("N = End Motor")
    print("F = Forward Motor")
    print("B = Backward Motor")
    inv = input("Your Choice: ")
    for n in range(1):
        sendChar(inv.upper())
    if inv.upper() == "G":
        print(getData())
        for n in range(0,total_element):
            myStrippedString = getData()
            print(myStrippedString)
            # split on the commas
            mySplitStrings = myStrippedString.split(',')

            # Convert entries to numbers from strings
            print(myStrippedString)
            myNum1 = float(mySplitStrings[0])
            times[n] = myNum1
            myNum2 = float(mySplitStrings[1])
            values[n] = myNum2
            myNum3 = float(mySplitStrings[2])
            velocity[n] = myNum3
        plot_graph(times,values,"Position (Deg)","Position Reference")
        plot_graph(times,velocity,"Velocity (RPM)","Velocity Reference")
        create_csv(times,values,velocity,total_element)
        j = calculate_j(values,velocity,total_element)
        print("J = ",j)
    elif inv.upper() == "Z":
        print(getData())
    elif inv.upper() == "P":
        print(getData())
        pos = int(getData())
        print('Encoder Position : ',pos)
    elif inv.upper() == "D":
        print(getData())
        delta = int(getData())
        print('Encoder Delta : ',delta)
    elif inv.upper() == "T":
        print(getData())
    elif inv.upper() == "M":
        print(getData())
    elif inv.upper() == "N":
        print(getData())
    elif inv.upper() == "F":
        print(getData())
    elif inv.upper() == "B":
        print(getData())
    elif inv.upper() == "S":
        print(getData())
        break

ser.close()

#create_csv(times,values,total_generate)
