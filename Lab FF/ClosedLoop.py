# -*- coding: utf-8 -*-

class ClosedLoop:
    
    def __init__(self):
        self.saturation_up_limits = 300.0
        self.saturation_down_limits = -300.0
        self.Kp = 1
        ## Testing the Full Omega Reference 
        self.omega_reference = 100
        # L = Kp * (self.omega_reference - get_delta)
        # L = 1 * (100-90)
        # L = 1 * (100-80)
        self.L_percent = 100.0        
        #print('Creating a ClosedLoop Object')
        
    def update(self,omega):
        L_percent = self.Kp * (self.omega_reference - omega)
        if L_percent > self.saturation_up_limits:
            L_percent = self.saturation_up_limits
        elif L_percent < self.saturation_down_limits:
            L_percent = self.saturation_down_limits
        self.L_percent = L_percent
        return self.L_percent
        
    def set_Kp(self,Kp):
        self.Kp = Kp
        
    def get_Kp(self):
        return self.Kp

    def get_L_percent(self):
        return self.L_percent