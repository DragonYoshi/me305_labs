# -*- coding: utf-8 -*-
from array import array
from math import exp, sin, pi

# Generate a few arrays to hold the data to plot. Here the arrays
# are set to use floats as the data type by specifying 'f' as the
# first argument. 

def generate_times(start_pos,end_pos):
    times  = array('f', list(time/10 for time in range(start_pos,end_pos)))
    return times

# We need a place to store the data values as well.
def generate_values(times,total_element):
    values = array('f', total_element*[0])
    n=0
    # We could do this with an iterator, but a for loop is OK too.
    for time in times:
        values[n] = exp(-time/10)*sin(2*pi/3*time)
        n += 1
    return values

