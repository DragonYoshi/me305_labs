# -*- coding: utf-8 -*-

import pyb

class MotorDriver:
    
    def __init__(self, nSLEEP_pin, pin_IN1, pin_IN2, pin_IN3,pin_IN4,tim):
        self.tch1 = tim.channel(1,pyb.Timer.PWM, pin=pin_IN1)
        self.tch2 = tim.channel(2,pyb.Timer.PWM, pin=pin_IN2)
        self.tch3 = tim.channel(3,pyb.Timer.PWM, pin=pin_IN3)
        self.tch4 = tim.channel(4,pyb.Timer.PWM, pin=pin_IN4)
        print('Creating a motor driver')
        
    def enable(self,pin_nSLEEP):
        # Both High or low Stop
        # ONe High, one Low Motor Move
        pin_nSLEEP.high()
        
    def disable(self,pin_nSLEEP):
        pin_nSLEEP.low()
        
    def set_duty(self,pin_IN1,pin_IN2,duty):
        if duty > 0:
            pin_IN1.high()
            pin_IN2.low()
            self.tch1.pulse_width_percent(80)
            self.tch2.pulse_width_percent(0)
            #width_per = duty
        else:
            pin_IN1.low()
            pin_IN2.high()
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(80)
            #width_per = -1 * duty

        #self.tch1.pulse_width_percent(width_per)
        #self.tch2.pulse_width_percent(width_per)
        
