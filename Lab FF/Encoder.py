# -*- coding: utf-8 -*-
import pyb


class Encoder:
    
    def __init__(self,pin_EA,pin_EB,tim):
            
        self.tim = tim
        self.tch1 = self.tim.channel(1,pyb.Timer.ENC_A, pin=pin_EA)
        self.tch2 = self.tim.channel(2,pyb.Timer.ENC_B, pin=pin_EB)
        self.current_counter = self.tim.counter() 
        self.prev_counter = self.tim.counter()
            
    def update(self):
        self.prev_counter = self.current_counter
        self.current_counter = self.tim.counter()
            
    def get_position(self):

        self.update()        
        return self.tim.counter()
    
    def set_position(self,number):
        
        self.update()
        self.current_counter = 0
        self.tim.counter(number)
        
    def get_delta(self):
        
        if self.current_counter > 49152 and self.prev_counter < 16383:
            delta = (-1 * (65535 - self.current_counter)) - self.prev_counter
        elif self.current_counter < 16383 and self.prev_counter > 49152:
            delta = self.current_counter - (-1 *(65535 - self.prev_counter))
        else:    
            delta = self.current_counter - self.prev_counter
        return delta
        