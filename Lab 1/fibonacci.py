# -*- coding: utf-8 -*-
## @file fibonacci.py
#  Fibonacci Number
#
"""
Created on Mon Jan 18 17:01:28 2021

@file: fibonacci.py
@author: Joey Chen
"""

''' @ file fibonacci.py
'''

def fib(idx):
    
    ## Fibonacci Function
    prev_n2 = 0
    prev_n1 = 1
    counter = 1
    if idx == 0:
        return 0;
    elif idx == 1:
        return 1
    else:
        while(counter < idx):
            x = prev_n2 + prev_n1
            counter += 1
            prev_n2 = prev_n1
            prev_n1 = x
        return x

if __name__ == '__main__':
    
    ## User interface that allow user to input the index of fibonacci series
    ## This index is to validate and return appropriate error messages 
    ## when it is invalid
    
    continue_flag = 'Y'
    while continue_flag.upper() == 'Y':
        idx = 0

        while True:
            try:
                idx = int(input('Please enter a fibonacci index '))
                if idx >= 0:
                    break
                else:
                    print('Index must be 0 or positive')
            except ValueError:
                print("Fibonacci index invalid")


        ## Print the corresponding fibonacci number with index
    
        print('Fibonacci numer at index {:} is {:}.'.format(idx,fib(idx)))
        continue_flag = input("Select another index (Y/N) ")
        
    
