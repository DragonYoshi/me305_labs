## @file elevator.py
#  Brief doc for elevator.py
#
#  Detailed doc for elevator.py 
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date January 25, 1970
#
#  @package Elevator
#
#  Brief doc for the elevator FSM (finite state machine) module
#
#  Detailed doc for the elevator fSM module
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date January 25, 1970

## A Elevator FSM Object 
#
#  Details
#  @author Joey Chen
#  @copyright License Info
#  @date January 25, 1970
 
import time
import random

class Elevator_FSM:
    
    ## Constructor for Elevator FSM
	#

    def __init__(self):
        # initializing states
        self.button_1 = 1
        self.button_2 = 0
        self.motor = 0
        self.first_floor = 1
        self.second_floor = 0
        self.current_state = 0
        self.stopped = False

	## Press Button 1 for the Elevator
	#
	#  Detailed info on button 1 Pressed function
	#

    def button1(self):
        if self.current_state == 3:
            self.button_1 = 1
            self.button_2 = 0
            self.motor = 2
            self.current_state = 0
            
	## Press Button 2 for the Elevator
	#
	#  Detailed info on button 2 Pressed function
	#

    def button2(self):
        if self.current_state == 1:
            self.button_2 = 1
            self.button_1 = 0
            self.motor = 1
            self.current_state = 2

	## Elevator Moving Down
	#
	#  Detailed info about Moving Down function
	#

    def move_down(self):
        self.button_1 = 0
        self.motor = 0
        self.first_floor = 1
        self.current_state = 1
        
	## Elevator Moving Up
	#
	#  Detailed info about Moving Up function
	#

    def move_up(self):
        self.button_2 = 0
        self.motor = 0
        self.second_floor = 1
        self.current_state = 3
                  
if __name__ == '__main__':
    
    ## User interface that allow user to input the index of fibonacci series
    #  This index is to validate and return appropriate error messages 
    #  when it is invalid
    
    Elevator = Elevator_FSM()
    button = 1
    print("Welcome to Elevator FSM")
    print('Press CTRL-C at any time to end the machine')
    print('')
    
    while True:
        time.sleep(1)
        if Elevator.current_state == 0:
            print('Moving Down')
            Elevator.move_down()
        elif Elevator.current_state == 1:
            print('Stopped on Floor 1')
            button = random.randint(1,2)
            if button == 1:
                Elevator.button1()
            else:
                Elevator.button2()
        elif Elevator.current_state == 2:
            print('Moving Up')
            Elevator.move_up()            
        elif Elevator.current_state == 3:
            print('Stopped on Floor 2')
            button = random.randint(1,2)
            if button == 1:
                Elevator.button1()
            else:
                Elevator.button2()
        else:
            continue
        
        