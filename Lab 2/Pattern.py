## @file pattern.py
#  Brief doc for pattern.py
#
#  Detailed doc for pattern.py 
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date January 27, 1970
#
#  @package Patternr
#
#  Brief doc for the CPU Pattern
#
#  Detailed doc for the pattern module
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date January 27, 1970

## A Pattern Object 
#
#  Details
#  @author Joey Chen
#  @copyright License Info
#  @date January 27, 1970

import pyb
import micropython
import time

##   micropython.alloc_emergency_exception_buf(100)

class Pattern:
    
    ## Constructor for Pattern FSM
	#

    def __init__(self):
        # initializing states
        self.current_state = 1

	## Press Button 1 for the Elevator
	#
	#  Detailed info on button 1 Pressed function
	#

    def next_state(self):
        if self.current_state == 1:
            self.current_state = 2
        elif self.current_state == 2:
            self.current_state = 3
        else:
            self.current_state = 1

## Pattern 1

def Pattern_1():
        
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    
    pinC13.value(1)
    ButtonInt.enable()

    # The Square Wave Pattern 
    # Duration : 1 second = 1000 ms
    # First 500 ms in the period is the highest brightness 100
    # The second 500 ms in the period is the lowest brigthness 0
    #    
    while True:
        now = time.ticks_ms()
        cycle = (now % 1000)
        if cycle < 500:
            pinA5.high()
        else:
            pinA5.low()
        if pinC13.value() == 0:
            while True:
                pinC13.value(1)
                if pinC13.value() == 1:
                    break
            break

#       
## pattern 2
def Pattern_2():

    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    pinC13.value(1)
    ButtonInt.enable()
    
    # The Sinewave Pattern 
    # Duration : 10 second = 10000 ms
    # Every 50 ms, change brightness by 1
    # The Brightness for time 0 is 50
    # The highest brightness 100 is on the 2500 ms
    # The lowest brightness 0 is on the 7500 ms
    #
    while True:
        now = time.ticks_ms()
        cycle = ((now % 10000) // 50) % 100
        if cycle < 50:
            brightness = 50 + cycle
        else:
            brightness = 100 - cycle
        t2ch1.pulse_width_percent(brightness)
        if pinC13.value() == 0:
            while True:
                pinC13.value(1)
                if pinC13.value() == 1:
                    break
            break        

## pattern 3
def Pattern_3():
    
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    pinC13.value(1)
    ButtonInt.enable()

    # The Triangle Wave Pattern 
    # Duration : 1 second = 1000 ms
    # Start at the lowest brightness 0, and increase the brightnes by 1
    # for every 10 ms
    #    
    while True:
        now = time.ticks_ms()
        brightness = 1 * ((now % 1000) // 100)
        t2ch1.pulse_width_percent(brightness)
        if pinC13.value() == 0:
            while True:
                pinC13.value(1)
                if pinC13.value() == 1:
                    break
            break

def OnButtonPressFCN(IRQ_src):
    print('Button Pressed')
    ButtonInt.disable()
    pinC13.value(1)
    
if __name__ == '__main__':
    
    ## User interface that allow user to input the index of fibonacci series
    #  This index is to validate and return appropriate error messages 
    #  when it is invalid

    ## -----------------------
    micropython.alloc_emergency_exception_buf(100)
    ## -----------------------
    ## pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ## tim2 = pyb.Timer(2, freq = 20000)
    ## t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)
    ## ButtonInt = pyb.ExtInt(pinC13,mode = pyb.ExtInt.IRQ_FALLING, 
    ##                       pull=pyb.Pin.PULL_NONE, callback=OnButtonPressFCN)
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13,mode = pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=OnButtonPressFCN)
        
    Patterns = Pattern()
    print('Welcome to Pattern FSM')
    print('Press Button to toggle next pattern')
    print('Press CTRL-C to end the machine')
    print('')
    
    while True:
        
        if Patterns.current_state == 1:
            print("Square Wave Pattern selected")
            Pattern_1()
            Patterns.next_state()
        elif Patterns.current_state == 2:
            print('Sinewave Pattern selected')
            Pattern_2()
            Patterns.next_state()
        elif Patterns.current_state == 3:
            print('Triangle Wave Pattern selected')
            Pattern_3()
            Patterns.next_state()
        else:
            break

        
        