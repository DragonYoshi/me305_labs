## @file SimonSay.py
#  Brief doc for SimonSay.py
#
#  Detailed doc for SimonSay.py 
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date February 10, 1970
#
#  @package SimonSay
#
#  Brief doc for the Simon Say Module
#
#  Detailed doc for the Simon Say module
#
#  @author Joey Chen
#
#  @copyright License Info
#
#  @date February, 1970

## A Simon Say Object 
#
#  Details
#  @author Joey Chen
#  @copyright License Info
#  @date February 10, 1970

import pyb
import micropython
import time
import random

##   micropython.alloc_emergency_exception_buf(100)

class Sequence:
    
    ## Constructor for Sequence
	#

    def __init__(self):
        # initializing states
        self.current_state = 1
        # Predefined Word List (10 words), each word consist 3 letters
        # We cam easily increase the word list by adding new element in
        # the list table
        self.word_level1 = [['S','.','O','.','S','/'],
                            ['A','.','R','.','E','/'],
                            ['S','.','O','.','S','/'],
                            ['T','.','I','.','E','/'],
                            ['D','.','I','.','E','/'],
                            ['G','.','E','.','M','/'],
                            ['S','.','O','.','N','/']]
        self.word_level2 = [['B','.','A','.','L','.','L','/'],
                            ['F','.','I','.','S','.','H','/'],
                            ['C','.','E','.','L','.','L','/'],
                            ['H','.','I','.','D','.','E','/'],
                            ['S','.','E','.','A','.','L','/'],
                            ['L','.','O','.','O','.','K','/'],
                            ['B','.','E','.','A','.','N','/']]
        self.word_level3 = [['A','.','P','.','P','.','L','.','E','/'],
                            ['D','.','R','.','I','.','V','.','E','/'],
                            ['G','.','R','.','E','.','A','.','T','/'],
                            ['H','.','E','.','A','.','R','.','T','/'],
                            ['P','.','R','.','I','.','C','.','E','/'],
                            ['S','.','H','.','O','.','O','.','T','/'],
                            ['W','.','E','.','A','.','V','.','E','/']]

	## Get a word from a predefined word list
	#
	#  Detailed info on Get Sequence
	#
            
    def GetWord(self,state,difficult_lvl):
        if difficult_lvl == 1:
            return self.word_level1[state]
        elif difficult_lvl == 2:
            return self.word_level2[state]
        elif difficult_lvl == 3:
            return self.word_level3[state]
        else:    
            return self.word_level1[state]
       
## Display Sequence Routine
#  Passing a sequence and display the sequence every xx ms (xx can be defined
#  in Constant module_factor and period). 
#  Constant
#         module_factor : the time unit to display the sequence
#         period        : the time unit to display one sequence
#  Parameter
#         Display_seq: the current sequence
    
def Display_Sequence(display_seq):
     
    module_factor = 600
    period = module_factor // 2
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    
    pinC13.value(1)
    ButtonInt.enable()
    
    idx = 0
    now = time.ticks_ms()
    cycle = (now % module_factor)
    if cycle < period:
        time_frame = 1
    else:
        time_frame = 2
        
    while idx < len(display_seq):
        if display_seq[idx] == 1:
            pinA5.high()
        else:
            pinA5.low()
        now = time.ticks_ms()
        cycle = (now % module_factor)
        if cycle < period:
            if time_frame == 2:
                idx += 1
            time_frame = 1
        else:
            if time_frame == 1:
                idx += 1
            time_frame = 2

    pinA5.low()
    return 0

## Convert Sequence Routine
#  Convert a letter from list to morse code sequence
#  Passing a sequence and convert to Morse code 
#  This program only convert alphabet A-Z and space between letters
#  Parameter
#         Display_seq: the current letters

def Convert(display_seq):
    current_seq = []
    for idx in range(0,len(display_seq)):
        if display_seq[idx] == 'A':
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'B':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'C':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'D':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'E':   
           current_seq.append(1)
        elif display_seq[idx] == 'F':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'G':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'H':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'I':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'J':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'K':  
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)            
        elif display_seq[idx] == 'L': 
           current_seq.append(1)
           current_seq.append(0)            
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'M':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'N':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == "O":
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)            
           current_seq.append(1)
           current_seq.append(1)           
        elif display_seq[idx] == 'P':   
           current_seq.append(1)
           current_seq.append(0)            
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'Q':  
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)            
        elif display_seq[idx] == 'R':   
           current_seq.append(1)
           current_seq.append(0)            
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'S':
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
        elif display_seq[idx] == 'T':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'U':  
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)            
        elif display_seq[idx] == 'V':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'W':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'X':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)            
        elif display_seq[idx] == 'Y':   
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == 'Z':   
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(0)
           current_seq.append(1)
           current_seq.append(1)
           current_seq.append(1)
        elif display_seq[idx] == '.':
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
        elif display_seq[idx] == '/':
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
            current_seq.append(0)
        else:
            current_seq.append(0)
    #print(current_seq)
    return current_seq

## Game Play Routine
#  Passing a sequence and let the user to push the button to duplicate the
#  sequence display before.
#  If the user press the button at wrong time, it will break the routine
#  and display failure message and back to main routine.
#  Parameter
#         sequence: the current sequence

def GamePlay(sequence):
        
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    
    pinC13.value(1)
    ButtonInt.enable()
    
    #print("Sequence all ",sequence)
    total_len = len(sequence)
    idx = 0
    begin_flag = False
    now = time.ticks_ms()    
    while idx < total_len:                                                
        if pinC13.value() == 0:
            then = time.ticks_ms()
            elapsed_time = then - now
            #print("Idle Time ",idx,"--> ",elapsed_time)         
            if begin_flag == True:
                if elapsed_time > 0 and elapsed_time <= 1000:
                    if sequence[idx] == 0:
                        idx += 1
                    else:
                        print("Button should press at this point")
                        return 1
                elif elapsed_time > 1000 and elapsed_time <= 1200:
                    print("Idle time too long")
                    return 1
                elif elapsed_time > 1200 and elapsed_time <= 2500:
                    if idx + 2 < total_len and \
                        sequence[idx] == 0 and \
                        sequence[idx+1] == 0 and \
                        sequence[idx+2] == 0:
                        idx += 3
                    else:
                        print("Button should not be pressed at this point")
                        return 1
                elif elapsed_time > 2500 and elapsed_time <= 3000:
                    print("Idle time too long")
                    return 1                        
                elif elapsed_time > 3000 and elapsed_time <= 4200:
                    if idx + 6 < total_len and \
                        sequence[idx] == 0 and \
                        sequence[idx+1] == 0 and \
                        sequence[idx+2] == 0 and \
                        sequence[idx+3] == 0 and \
                        sequence[idx+4] == 0 and \
                        sequence[idx+5] == 0 and \
                        sequence[idx+6] == 0:
                            idx += 7
                    else:
                        print("Button should not be pressed at this point")
                        return 1
                else:
                    print("Idle time too long")
                    return 1
            begin_flag = True
            now = time.ticks_ms()    
            while True:
                pinA5.high()
                pinC13.value(1)
                if pinC13.value() == 1:
                   break
            then = time.ticks_ms()
            elapsed_time = then - now
            #print("Elapsed Time: ",idx," --> ",elapsed_time)               
            if elapsed_time > 0 and elapsed_time <= 250:
                if sequence[idx] == 1:
                    idx += 1
                else:
                    if idx + 1 < total_len:
                        if sequence[idx+1] == 1:
                            print("Button should be pressed longer")
                            return 1
                    else:
                        print("Button should not be pressed at this point")
                        return 1
            elif elapsed_time > 250 and elapsed_time < 1500:
                if idx + 2 < total_len and \
                    sequence[idx] == 1 and \
                    sequence[idx+1] == 1 and \
                    sequence[idx+2] == 1:
                    idx += 3
                else:
                    print("Button should not be pressed at this point")
                    return 1
            else:
                print("You pressed the button for too long")
                return 1
            pinA5.low()
            now = time.ticks_ms()
        else:
            pinA5.low()
    return 0

## Trucate Trailing zero 
#  The routine will trucate the trailing 0 sequence.
#  Parameter
#         sequence: the current sequence

def trucate_trailing_zero(sequence):
    new_seq = []
    for idx in range(len(sequence),0,-1):
        if sequence[idx-1] == 1:
            break
    last_idx = (idx)
    for i in range(0,last_idx):
        new_seq.append(sequence[i])
    return new_seq

## Button Press interrupt routine
#  Button pressed routine 

def OnButtonPressFCN(IRQ_src):
    print('Button Pressed')
    ButtonInt.disable()
    pinC13.value(1)
    
if __name__ == '__main__':
    
    ## Implement a "Simon Says" game using Nucleoo 1476 board utilizing the
    #  User LED (LED2) and the User Button (B1). 

    ## -----------------------
    micropython.alloc_emergency_exception_buf(100)
    ## -----------------------
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13,mode = pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=OnButtonPressFCN)
        
    Sequence = Sequence()
    
    # Welcome and Game rule message
    print("Welcome to the Simon Says Game")
    print(" ")
    print("This game ask you to reproduce the sequence that the")
    print("LED2 display, the sequence will be shown twice before you start.")
    print("After that, you need to reproduce the pattern by pushing the")
    print("button in the identical sequence. If you push the button at the ")
    print("wrong time, you lose.")
    print(" ")
    print("The sequence will start with one third of the total pattern, if you")
    print("pass, it will increase to two third of the total pattern, and ")
    print("then the full pattern. If you pass all three levels, you win!")
    print('')
    print("Please choose a level between 1 and 3")
    
    while True:
        try:
            difficult_level = int(input('Level of Difficulty (1=Easy, 2=Medium, 3=Hard): '))
            if difficult_level >= 1 and difficult_level <= 3:
                break
            else:
                print('Level of difficulty must between 1 and 3')
        except ValueError:
            print("Invalid Input")
    
    # Constant
    # partition : Total Partition for show sequence
    # predefined_sequence : Total number of predefined sequence
    if difficult_level == 1:
        partition = 3
    elif difficult_level == 2:
        partition = 4
    elif difficult_level == 3:
        partition = 5
    else:
        partition = 3
    partition_size = 2
    predefined_sequence = 6
    
    total_game = 0
    total_win = 0
    continue_flag = 'Y'
    
    while continue_flag.upper() == 'Y':
        pass_time = 0
        total_game += 1
        state = random.randint(1,predefined_sequence)
        current_seq = Sequence.GetWord(state,difficult_level)
        for i in range(0,partition):
            display_seq = current_seq[0:partition_size*(i+1)]
            convert_seq = Convert(display_seq)
            print("The sequence will be shown twice")
            print("Memorize the sequence and recreate it ")
            print(" ")
            print("Phase : ",i+1)
            for j in range(0,2):
                time.sleep(1)
                print("Blink Sequence ",j+1)
                winning = Display_Sequence(convert_seq)
                time.sleep(1)
            print("Your Turn")
            print(" ")
            time.sleep(1)
            sequence = trucate_trailing_zero(convert_seq)
            #print(sequence)
            OK_flag = GamePlay(sequence)
            if OK_flag == 0:
                pass_time += 1
                print("Phase ",i+1," Complete. Next Phase.")
            else:
                print("Game Over")
                break
        if pass_time == partition:
            print("Congratulations you win!")
            total_win += 1
        continue_flag = input("Play another Game? (Y/N) ")
        
    # print game playing and win/loss record     
    print(" ")
    print("Games Played : ",total_game)
    print("Games Won    : ",total_win)
    print("Games Lost   : ",total_game - total_win)